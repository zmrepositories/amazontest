package nl.hanze.movieshowtime.processor.external.amazon;

public class AmazonTest {

	public static void testClient() {

		//instantiate a new URLhelper
		URLHelper urlHelper = new URLHelper();
		
		//get a signed url from the URLhelper with the search keyword
		String signedUrl = urlHelper.getSignedUrl("The Hunger Games");

		//fetch first title from xml response
		String title = urlHelper.fetchTitle(signedUrl);
		System.out.println("title from response = " + title);

		//fetch first actor from xml response
		String actor = urlHelper.fetchActor(signedUrl);
		System.out.println("actor from response = " + actor);

	}
	
	

	public static void main(String[] args) throws Exception {

		AmazonTest.testClient();

	}

}
