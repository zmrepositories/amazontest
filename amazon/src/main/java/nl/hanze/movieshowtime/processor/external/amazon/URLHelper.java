package nl.hanze.movieshowtime.processor.external.amazon;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

/*
 * This class shows how to make a simple authenticated ItemLookup call to the
 * Amazon Product Advertising API.
 * 
 * See the README.html that came with this sample for instructions on
 * configuring and running the sample.
 */
public class URLHelper {
    /*
     * Your AWS Access Key ID, as taken from the AWS Your Account page.
     */
    private static final String AWS_ACCESS_KEY_ID = "AKIAJP33LQB3NU7GACPQ";

    /*
     * Your AWS Secret Key corresponding to the above ID, as taken from the AWS
     * Your Account page.
     */
    private static final String AWS_SECRET_KEY = "9oWboam+IrQLMmIKEfptI12I27AKO1UIhUwFtC+P";
    
    /*
     * You Associate Tag from amazon
     */
    private static final String ASSOCIATE_TAG="school021d-20";

    /*
     * Use one of the following end-points, according to the region you are
     * interested in:
     * 
     *      US: ecs.amazonaws.com 
     *      CA: ecs.amazonaws.ca 
     *      UK: ecs.amazonaws.co.uk 
     *      DE: ecs.amazonaws.de 
     *      FR: ecs.amazonaws.fr 
     *      JP: ecs.amazonaws.jp
     * 
     */
    private static final String ENDPOINT = "ecs.amazonaws.co.uk";

    /*
     * The keyword to lookup. 
     * You can choose a different value if this value does not work in the
     * locale of your choice.
     */
    private String keyword = null;
   

    public String getSignedUrl(String Keyword) {
        /*
         * Set up the signed requests helper 
         */
    	
    	this.keyword = Keyword;
        SignedRequestsHelper helper;
        try {
            helper = SignedRequestsHelper.getInstance(ENDPOINT, AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        
        String requestUrl = null;
        

        /* The helper can sign requests in two forms - map form and string form */
        
        /*
         * Here is an example in map form, where the request parameters are stored in a map.
         */
        Map<String, String> params = new HashMap<String, String>();
        params.put("Service", "AWSECommerceService");
        params.put("Version", "2011-08-01");
        params.put("AssociateTag", ASSOCIATE_TAG);
        params.put("Operation","ItemSearch");
        params.put("SearchIndex","DVD");
        params.put("Keywords",keyword);
        params.put("ResponseGroup","Medium");
       

        requestUrl = helper.sign(params);
      
        /* Here is an example with string form, where the requests parameters have already been concatenated
         * into a query string. */
       
        String queryString = "AWSAccessKeyId="+AWS_ACCESS_KEY_ID+"&AssociateTag="+ASSOCIATE_TAG+"&Keywords="+keyword+"&Operation=ItemSearch&ResponseGroup=Medium&SearchIndex=DVD&Service=AWSECommerceService&Timestamp=2015-01-23T11%3A51%3A45.000Z&Version=2011-08-01";
        
        
        requestUrl = helper.sign(queryString);    
       
        System.out.println("url for request = "+requestUrl);
		return requestUrl;

    }

    /*
     * Utility function to fetch the response from the service and extract the
     * title from the XML.
     */
    public String fetchTitle(String requestUrl) {
        String title = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(requestUrl);
            Node titleNode = doc.getElementsByTagName("Title").item(0);
            title = titleNode.getTextContent();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return title;
    }
    
    /*
     * Utility function to fetch the response from the service and extract the
     * title from the XML.
     */
    public String fetchActor(String requestUrl) {
        String actor = null;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(requestUrl);
            Node actorNode = doc.getElementsByTagName("Actor").item(0);
            actor = actorNode.getTextContent();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return actor;
    }

}
